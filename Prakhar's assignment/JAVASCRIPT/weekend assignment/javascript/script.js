

var givenObject = [
    {
    "fileName": "flower",
    "type": "jpg"
    },
    {
    "fileName": "family-video-clip",
    "type": "mp4"
    },
    {
    "fileName": "phone-ringtone",
    "type": "mp3"
    },
    "javascript-exercises.txt",
    "learning-html-basics.rtf",
    {
    "fileName": "friend-video-clip",
    "type": "mp4"
    },
    {
    "fileName": "resume",
    "type": "docx"
    },
    {
    "fileName": "student-report",
    "type": "csv"
    },
    {
    "fileName": "sms-ringtone",
    "type": "mp3"
    },
    "html-basics.pdf",
    "dubsmash.mp4",
    "screen-shot.png",
    {
    "fileName": "faculty-report",
    "type": "xlsx"
    },
    {
    "fileName": "puppy",
    "type": "svg"
    }];

    var audioFormat  = ["m4a","flac","mp3","mpeg-4","wav","wma","aac"];
    // var audioFiles = {};

    var videoFormat = ["webm","mpg","ogg","mp4","avi","mov","mkv","flv","wmv","avchd"];
    // var videoFiles = {};

    var imageFormat = ["svg","jpeg","jpg","png","gif","tiff","psd","eps","ai","bmp","raw"];
    // var imageFiles = {};

    var documentFormat = ["rtf","csv","doc","docx","txt","pdf","html","ppt","pptx","odt","xls","xlsx"];
    // var documentFiles = {};
    var segregatedFormats = {
        "audio":[],
        "video":[],
        "image":[],
        "document":[]
    };


var audioIndex = 0;
var videoIndex = 0;
var imageIndex = 0;
var documentIndex = 0; 



function addFile(fileType,file){
    let flag = false;
    switch(fileType){
        case "audio":
            
            for ( let present of Object.keys(segregatedFormats.audio))
            if(segregatedFormats.audio[present] == file)
            flag = true;
            if(flag == false)
            segregatedFormats.audio[audioIndex++] = file;
            flag = false;
            break;
        case "video":
            // let flag = false;
            for ( let present of Object.keys(segregatedFormats.video))
            if(segregatedFormats.video[present] == file)
            flag = true;
            if(flag == false)
            segregatedFormats.video[videoIndex++] = file;
            flag =false;
            break;
        case "image":
            // let flag = false;
            for ( let present of Object.keys(segregatedFormats.image))
            if(segregatedFormats.image[present] == file)
            flag = true;
            if(flag == false)
            segregatedFormats.image[imageIndex++] = file;
            flag = false;
            break;
        case "document":
            // let flag = false;
            for ( let present of Object.keys(segregatedFormats.document))
            if(segregatedFormats.document[present] == file)
            flag = true;
            if(flag == false)
            segregatedFormats.document[documentIndex++] = file;
            flag  = false;
            break;

    }
}

    
let audioFlag = false,videoFlag = false,imageFlag = false,documentFlag = false;
 
function findFileType(value){
    
    let audioPos = 0,videoPos = 0,imagePos = 0,documentPos = 0;
    for( audioPos,videoPos,imagePos,documentPos;videoPos<=15;){
        if(audioFormat[audioPos] == value){
            audioFlag = true;
            break;
        }
        if(videoFormat[videoPos] == value){
            videoFlag = true;
            break;

        }
        if(imageFormat[imagePos] == value){
            imageFlag = true;
            break;

        }
        if(documentFormat[documentPos] == value){
            documentFlag = true;
            break;

        }
        // console.log(value);
        audioPos++;
        videoPos++;
        imagePos++;
        documentPos++;
    }
    if(audioFlag){
        audioFlag = false;
        return "audio";
    }
    if(videoFlag){
        videoFlag = false;
        return "video";
    }
    if(imageFlag){
        imageFlag = false;
        return "image";
    }
    if(documentFlag){
        documentFlag = false;
        return "document";
    }
    return `invalid type for ${value}`;
}
   
 
function segregateFiles(){   

for ( let key of Object.keys(givenObject)){
    for ( let key1 of Object.keys(givenObject[key])){
        if(typeof(givenObject[key]) == 'object'){
            let value = givenObject[key].type;
            let fileType = findFileType(value);
            addFile(fileType,givenObject[key].fileName);
            
        }
        else{
            // use regular expression here.
            let str = givenObject[key];
            let patt = /\w\.\w*/
            let patt2 = /(.*\.)/
            let arr = patt.exec(str)[0];
            let extension  = arr.split('.')[1];
            let arr2 = patt2.exec(str)[0];
            let originalFileName = arr2.split('.')[0];
            let fileType = findFileType(extension);
            addFile(fileType,originalFileName);
            // console.log(originalFileName);
            
        }
    }
    
}
}

segregateFiles();
for (let key of Object.keys(segregatedFormats)){
document.write(`"${key}":<br>[`);
    for ( let key1 of Object.keys(segregatedFormats[key])){
        document.write(`${segregatedFormats[key][key1]},&nbsp`);
    }
    document.write(`]<br>`)
}

var questionBank = {
    "0":"Click on a Circle shaped Object.",
    "1":"Click on a Rectangle shaped Object.",
    "2":"Click on a Square shaped Object",
    "3":"Click on a Tree.",
    "4":"Click on the Crow.",
    "5":"Click on a Flower Plant",
    "6":"Click on a Cloud.",
    "7":"Click on a Triangle shaped Object"
}

var answerKey = {
        "0":"circle.png",
        "1":"rect.png",
        "2":"square.jpg",
        "3":"tree.jpg",
        "4":"crow.jpg",
        "5":"flower.jpg",
        "6":"cloud.jpg",
        "7":"triangle.jpg"

}

function  calcSize(questionBank){
    let size = 0;
    let calc = ()=>{
        for(let key of Object.keys(questionBank))
        size += 1;
        return size;
    }
    return calc;
}

let getSize = calcSize(questionBank);

var randomSet =  new Set();
function getRandomNo(maxCount,givenSet){
    let number;
    for ( let value = 1;value<=(maxCount)+10;value++ ){
     number  = Math.floor(Math.random()*(maxCount+1)-1)+1;
     givenSet.add(number);
    }
}


var maxSize = getSize(questionBank);
getRandomNo(maxSize,randomSet);
var randomArr = Array.from(randomSet);
console.log(randomArr);

let questionIs = document.querySelector('#question-details');
let first_choice = document.querySelector('#first');
let second_choice = document.querySelector('#second')
let third_choice = document.querySelector('#third')
let fourth_choice = document.querySelector('#fourth')

let choiceSet = new Set();
getRandomNo(3,choiceSet);
var choiceArr  = Array.from(choiceSet);

function setChoices(index){
    for ( let i in choiceArr){
        switch(Number(i)){

            case 1:
                fourth_choice.src = `images/${answerKey[randomArr[index]]}`
                second_choice.src = `images/${answerKey[randomArr[0]]}`
                third_choice.src = `images/${answerKey[randomArr[1]]}`
                first_choice.src = `images/${answerKey[randomArr[2]]}`
                break;

            case 2:
                third_choice.src = `images/${answerKey[randomArr[index]]}`
                second_choice.src = `images/${answerKey[randomArr[0]]}`
                first_choice.src = `images/${answerKey[randomArr[1]]}`
                fourth_choice.src = `images/${answerKey[randomArr[2]]}`
                break;
            case 3:
                second_choice.src = `images/${answerKey[randomArr[index]]}`
                first_choice.src = `images/${answerKey[randomArr[0]]}`
                third_choice.src = `images/${answerKey[randomArr[1]]}`
                fourth_choice.src = `images/${answerKey[randomArr[2]]}`
                break;
        }
    }
    
}

var image_name;
let index = 0;
let flag = false;
let ansFlag = false;
function display(){
    if(index<maxSize-3)
    {
        if(flag == false)
        {
            if(image_name == answerKey[0]){
            console.log("correct answer")
            ansFlag = true;
            }
            else{
            console.log("Wrong answer");
            ansFlag = false;
            }
            questionIs.innerHTML = `${questionBank[randomArr[index]]}`;
            setChoices(index);
            flag = true;
            return;
        }
        if(answerKey[randomArr[index]] == image_name){
        console.log("right answer");
        ansFlag = true;
        }
        else{
        console.log("wrong answer");
        ansFlag = false;
        }
        index++;
        questionIs.innerHTML = `${questionBank[randomArr[index]]}`
        setChoices(index);
    }
    else{
        
        questionIs.innerHTML = "Game Over,Refresh the Page to Play Again...!"
        window.alert("Game Over!")
        window.location.reload();
        
        
    }
}

function startClicked(){
    // document.querySelector('.choices').display=visible; 
    questionIs.innerHTML = `${questionBank[0]}`;
    first_choice.src = `images/${answerKey[0]}`;
}
let counter = 0;
function clicked(name){
    counter++;
    let ans = document.querySelector(`#${name}`);
    let ansSrc = ans.src;
    let arr  = ansSrc.split('/')[4];
    image_name = arr;
    display();
    if(ansFlag == true){
        document.querySelector('#start').innerHTML = "Correct Answer!";
        setTimeout(function(){window.alert("You Win,You Have Selected the correct Object.")},500);
        setTimeout(function(){window.location.reload()},1000);
    }
    else{
        document.querySelector('#start').innerHTML = "Wrong Answer, Try Again!";
    }
    
}



let colorBank = {
    "20":"red",
    "18":"violet",
    "14":"green",
    "16":"yellow",
    "17":"blue"
}

let paddingValueArr = [];
function calcSize(colorBank){
    let size = 1;
    let func = ()=>{
        if(paddingValueArr.length == 0)
        for ( let key of Object.keys(colorBank)){
            paddingValueArr[size-1] = Number(key);
            size++;
        }
        return size;
    }
    return func;
}

let getSize = calcSize(colorBank);

let randomDigitSet = new Set();
function getRandomDigit(maxSize){
    let value;
        for(let start = 1;start<=maxSize+10;start++){
        value  = Math.floor(Math.random()*((maxSize - 1) + 1));
        randomDigitSet.add(value);
    }
}
getRandomDigit(getSize(colorBank)-1);
let randomDigitArr = Array.from(randomDigitSet);

function getRandomPixels(flag,startX,endX){
    let value;
    value = Math.floor(Math.random()*(endX - startX +1)) + startX;
    return value;
}

var keyArr = [];
let index = 0;
for ( let key in colorBank){
keyArr[index++] = Number(key);
}


let colorValue="cyan";
let currentScore = 0;
function increasePoint(){
    
    let scoreElement = document.querySelector('.result-section h3');
    function increase(){
        switch(colorValue){
            case "red":
                currentScore -= 2;
                break;

            case "violet":
                currentScore -= 1;
                break;

            case "green":
                currentScore += 3;
                break;

            case "blue":
                currentScore += 1;
                break;

            case "yellow":
                currentScore += 2;
                break;        

            default:
                window.prompt("invalid color encountered!");
                break;
        }
        scoreElement.innerHTML = `Score : ${currentScore}`;
    }
    return increase;
}


let startClicket = false;
function display(){
    let startX = document.querySelector('#circle').style.left;
    let startY = document.querySelector('#circle').style.top;    
    let topValue,leftValue;
    let paddingValue;
    let arrIndex = 0;

    function displayCircle(){
        if(startClicket == false){
        window.alert("Please first Start the game")
        }
        else{
        if(arrIndex >= getSize(colorBank)-1)
        {
        arrIndex = 0;
        }
        paddingValue = Number(keyArr[arrIndex]);
        colorValue  = colorBank[paddingValueArr[randomDigitArr[arrIndex]]];
        console.log(`${colorValue} : ${randomDigitArr[arrIndex]}`);
        paddingValue = paddingValue;
        let xStart = document.querySelector('.game-body').getBoundingClientRect().width;
        let yStart = document.querySelector('.game-body').getBoundingClientRect().height;

        leftValue = Number(getRandomPixels(0,1,xStart-25));
        topValue = Number(getRandomPixels(1,1,yStart-25));

        let element = document.getElementById('circle');
        element.style.top = (startY + topValue)+"px";
        element.style.left  =(startX + leftValue)+"px";
        element.style.padding  = paddingValue+"px";
        element.style.background = colorValue;
        arrIndex++;
        }
    }
    return displayCircle;
}

let circleDisplay = display();

function stop(){
    window.alert(`Game Stopped! Score : ${currentScore}`);
    window.location.reload();
}

function gameOver(){
    if(counter.innerHTML == '60 Second'){
    window.alert(`Game Over! SCORE IS : ${currentScore}`);
    window.location.reload();
    }
}


let counter = document.querySelector('.result-section p');
let count = 59;

function timeOut(){
    counter.innerHTML = `${count} Second`;
}

function resetValue(){

    clearInterval(timeOut);
    counter.innerHTML = `60 Second`
    count = 60;
}

let waitTime = 1000;
function environment(){

        let circleObject = document.querySelector('#circle-radio');
        let squareObject = document.querySelector('#square');
        let levelEasy = document.querySelector('#easy');
        let levelMedium = document.querySelector('#medium');
        let levelHard = document.querySelector('#hard');
        let borderRad = document.querySelector('#circle');
        let setHeight = document.querySelector('.game-body');
        let setWidth = document.querySelector('.game-body');

        function chooseEnvironment(){
            
            if(circleObject.checked){
                borderRad.style.borderRadius = 50+"%";
            }
            else if (squareObject.checked){
                borderRad.style.borderRadius = 10+"%";
            }
            else{
                window.alert("Warning : You didn't select any custom Shape.")
                // continue;
            }

            if(levelEasy.checked){
                setHeight.style.height = 480+"px";
                setWidth.style.width = 600+"px";
                waitTime = 1200;
            }
            else if ( levelMedium.checked){
                setHeight.style.height = 600+"px";
                setWidth.style.width = 800+"px";
                waitTime =1000;
            }
            else if(levelHard.checked){
                setHeight.style.height = 750+"px";
                setWidth.style.width = 900+"px";
                waitTime = 750;
            }
            else{
                window.alert("Warning : You didn't select custom Level");
                // continue;
            }
            
        }
        return chooseEnvironment;
    }

let userGamingEnvironment = environment();
function timer(){
    userGamingEnvironment();
    if(Number(document.getElementById('duration').value)>1)
    {
    count = Number(document.getElementById('duration').value);
    }
    else
    {
    count = 59;
    }
    document.querySelector('.result-section p').innerHTML = Number(count)+"Seconds";
    startClicket = true; 
    window.scrollTo(0,document.body.scrollHeight);
    document.querySelector('.title h1').innerHTML = "Game Started..."
    setInterval(function(){timeOut();
    gameOver();
    circleDisplay();
    (count == -1)?resetValue():count = count-1;
    
    },waitTime);

}


function instruction(){
    window.open("instructions.html")
}